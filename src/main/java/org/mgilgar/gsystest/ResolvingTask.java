package org.mgilgar.gsystest;

import java.util.Queue;

import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.service.GameService;

/**
 * {@link Runnable} that executes the resolving process where the winners and loosers are calculated.
 * 
 * @author mgilgar
 *
 */
public class ResolvingTask implements Runnable {

	private Queue<Bet> bets;
	private GameService gameService;

	public Queue<Bet> getBets() {
		return bets;
	}

	public void setBets(Queue<Bet> bets) {
		this.bets = bets;
	}

	public GameService getGameService() {
		return gameService;
	}

	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}

	/**
	 * It executes the resolving process that calculates the winners and losers of every round and then prints out
	 * the result. It resets the bets for the next round.
	 *  
	 */
	@Override
	public void run() {
		System.out.println(gameService.resolve(bets));
		bets.clear();
	}
}
