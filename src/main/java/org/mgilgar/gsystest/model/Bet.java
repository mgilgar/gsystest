package org.mgilgar.gsystest.model;

import java.math.BigDecimal;

/**
 * Bet.
 * 
 * @author mgilgar
 *
 */
public class Bet {

	private BigDecimal amount;
	private String bet;
	private Player player;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBet() {
		return bet;
	}

	public void setBet(String bet) {
		this.bet = bet;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}
