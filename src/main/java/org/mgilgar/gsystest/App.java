package org.mgilgar.gsystest;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import org.mgilgar.gsystest.service.BetService;
import org.mgilgar.gsystest.service.GameService;
import org.mgilgar.gsystest.service.PlayerService;

/**
 * Command line wrapper for {@link Game}.
 * 
 * @author mgilgar
 *
 */
public class App {
	
	/**
	 * Executable method from command line 
	 * 
	 * @param args first arguments if the path of file containing {@link List} of {@link Player}.
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if (args==null || args.length==0) {
			System.err.println("No arguments provided. You should pass the file name that contains the name of the players.");
			return;
		}
		
		Game game = new Game();
		game.setPlayerService(new PlayerService());
		game.setBetService(new BetService());
		GameService gameService = new GameService();
		gameService.setRamdon(new Random());
		ResolvingTask resolving = new ResolvingTask();
		resolving.setGameService(gameService);
		game.setResolvingTask(resolving);
		game.setBetsInput(new BufferedReader(new InputStreamReader(System.in)));
		game.setPlayersInput(new BufferedReader(new FileReader(new File(args[0]))));
		game.play();
	}
}
