package org.mgilgar.gsystest.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.model.Player;

/**
 * Bet service. Operations to perform on bet objects.
 * 
 * @author mgilgar
 *
 */
public class BetService {

	private static final int MAXIMUM_NUMBER = 36;
	private static final String SEPARATOR = "\\s+";

	/**
	 * It builds a {@link Bet} based on the given input string
	 * @param input the string containing the bet as given by the user in the standard input.
	 * @return the corresponding {@link Bet} representing the input. If the bet is no between 1-36 or EVEN or ODD
	 * it returns null.
	 */
	public Bet buildBet(final String input) {
		if (input == null || input.equals("")) {
			return null;
		}
		String[] components = input.split(SEPARATOR);
		if (components.length != 3) {
			return null;
		}
		if (components[1].equals("EVEN") ||
				components[1].equals("ODD") ||
				(Integer.valueOf(components[1]) > 0 && Integer.valueOf(components[1]) <= MAXIMUM_NUMBER)) {
			Bet bet = new Bet();
			bet.setPlayer(new Player(components[0]));
			bet.setBet(components[1]);
			bet.setAmount(new BigDecimal(components[2]));
			return bet;
		} else {
			return null;
		}
	}

}
