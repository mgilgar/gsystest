package org.mgilgar.gsystest.service;

import java.math.BigDecimal;
import java.util.Queue;
import java.util.Random;

import org.mgilgar.gsystest.model.Bet;

/**
 * Game service. Contains operation
 * 
 * @author mgilgar
 *
 */
public class GameService {

	private static final String WIN = "WIN\t";
	private static final String LOSE = "LOSE\t";
	private static final String HEADER_SEPARATOR = "---\n";
	private static final String HEADER_NUMBER = "Number: ";
	private static final String HEADER = "Player\tBet\tOutcome\tWinnings\n";
	private static final int MAXIMUM_NUMBER = 36;
	private static final String NO_BETS_OUTPUT = "There are no bets in this game.";
	private static final String BET_EVEN = "EVEN";
	private static final String BET_ODD = "ODD";

	private Random random;
	
	public Random getRamdon() {
		return random;
	}

	public void setRamdon(final Random random) {
		this.random = random;
	}

	/**
	 * It calculates the amount of money won in a particular bet given a specific number.
	 * 
	 * @param bet the bet to calculate that amount of money won.
	 * @param number represents the place where the ball landed in the roulette.
	 * @return the amount of winnings corresponding to that bet and number.
	 */
	public BigDecimal getWinnings(Bet bet, int number) {
		if (bet.getBet().equals(BET_EVEN) && isEven(number)
				|| bet.getBet().equals(BET_ODD) && !isEven(number)) {
			return bet.getAmount().multiply(BigDecimal.valueOf(2));
		} else if (bet.getBet().equals(BET_EVEN) || bet.getBet().equals(BET_ODD)) {
			return BigDecimal.ZERO;
		} else if (Integer.valueOf(bet.getBet()).equals(number)) {
			return bet.getAmount().multiply(BigDecimal.valueOf(36));
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Given a queue of {@link Bet} it calculates who wins and who loses after throwing the ball
	 * and returns a report representing that result.
	 * 
	 * @param bets the bets to calculate the outcome.
	 * @return a string containing a report describing the outcome of the round.
	 */
	public String resolve(final Queue<Bet> bets) {
		if (bets == null || bets.size()==0) {
			return NO_BETS_OUTPUT;			
		}
		int number = throwBall();
		StringBuilder sb = new StringBuilder();
		sb.append(HEADER_NUMBER).append(number).append('\n')
		.append(HEADER)
		.append(HEADER_SEPARATOR);
		for (Bet bet: bets) {
			sb.append(resolve(bet, number));
		}
		return sb.toString();
	}
	
	/**
	 * It resolves the outcome of a given bet for a given number representing the place for the ball lands. 
	 * 
	 * @param bet the bet to resolve.
	 * @param number the number representing the place where ball landed.
	 * @return a report representing the outcome of the ball throwing for that given bet.
	 */
	protected StringBuilder resolve(final Bet bet, final int number) {
		StringBuilder sb = new StringBuilder();
		sb.append(bet.getPlayer().getName()).append('\t').append(bet.getBet()).append('\t');
		BigDecimal winnings = getWinnings(bet, number);
		if (winnings.equals(BigDecimal.ZERO)) {
			sb.append(LOSE);
		} else {
			sb.append(WIN);
		}
		sb.append(winnings.setScale(1)).append('\n');
		return sb;
	}
	
	private int throwBall() {
		return random.nextInt(MAXIMUM_NUMBER) + 1;
	}

	private boolean isEven(int number) {
		return number % 2 == 0;
	}
	
}
