package org.mgilgar.gsystest.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.mgilgar.gsystest.model.Player;

/**
 * Player Service. Operations to perform on Player objects.
 * 
 * @author mgilgar
 *
 */
public class PlayerService {

	/**
	 * It builds a {@link List} of {@link Player}s based on the given input.
	 * 
	 * @param in the input stream where the player are given.
	 * @return a {@link List} of {@link Player}s corresponding to the given input.
	 * @throws IOException if there is a problem reading the {@link BufferedReader}.
	 */
	public List<Player> getPlayers(final BufferedReader in) throws IOException {
		List<Player> players = new ArrayList<Player>();
		
		String s;
		while (( s = in.readLine()) != null && s.length() != 0) {
			players.add(new Player(s));
		}
		
		return players;
	}

}
