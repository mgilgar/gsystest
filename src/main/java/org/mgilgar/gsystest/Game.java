package org.mgilgar.gsystest;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.model.Player;
import org.mgilgar.gsystest.service.BetService;
import org.mgilgar.gsystest.service.PlayerService;

/**
 * Game. 
 * 
 * Implements the high level logic of the game.
 * 
 * @author mgilgar
 *
 */
public class Game {

	private static final int FREQUENCY = 30;
	private static final int INITIAL_DELAY_IN_SECONDS = 30;
	
	private BetService betService;
	private PlayerService playerService;
	private ResolvingTask resolvingTask;
	
	private Queue<Bet> bets;
	private List<Player> players;

	private BufferedReader betsInput;
	private BufferedReader playersInput;
	
	public void setBetService(final BetService betService) {
		this.betService = betService;
	}
	
	public void setPlayerService(final PlayerService playerService) {
		this.playerService = playerService;
	}

	public void setResolvingTask(final ResolvingTask resolvingTask) {
		this.resolvingTask = resolvingTask;
	}

	public void setBetsInput(final BufferedReader betsInput) {
		this.betsInput = betsInput;
	}

	public void setPlayersInput(final BufferedReader playersInput) {
		this.playersInput = playersInput;
	}

	/**
	 * It starts the game.
	 * 
	 * Implements the high level logic of the game.
	 * It reads the players from the file.
	 * It starts and schedules the resolving task that will calculate who has won the game.
	 * It starts the main loop that reads the bets.
	 * 
	 * @throws IOException
	 */
	public void play() throws IOException {
		players = playerService.getPlayers(playersInput);

		bets = new ConcurrentLinkedQueue<Bet>();		
		resolvingTask.setBets(bets);
		Executors.newScheduledThreadPool(1).scheduleAtFixedRate(resolvingTask, INITIAL_DELAY_IN_SECONDS, FREQUENCY, TimeUnit.SECONDS);
		
		processBets();
	}

	/**
	 * It reads bets fron the input and adds then to the list of bets if they come from the authorized list of players.
	 * 
	 * @throws IOException
	 */
	protected void processBets() throws IOException {
		String s;
		while (( s = betsInput.readLine()) != null && s.length() != 0) {
			Bet bet = betService.buildBet(s);
			if (bet==null) {
				System.err.println("You have bet an amount that is not allowed. Please use EVEN, ODD, 1-36.");
			} else {
				if (players.contains(bet.getPlayer())) {
					bets.add(bet);
				} else {
					System.err.println("We could not add the bet because the referred player does not play in this game.");
				}
			}
		}		
	}

	// For testing
	protected void setPlayers(final List<Player> players) {
		this.players = players;
	}

	// For testing
	protected Queue<Bet> getBets() {
		return bets;
	}
	
	// For testing
	protected void setBets(final Queue<Bet> bets) {
		this.bets = bets;
	}

	// For testing
	protected List<Player> getPlayers() {
		return this.players;
	}

}
