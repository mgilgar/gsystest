package org.mgilgar.gsystest;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mgilgar.gsystest.App;

/**
 * Test for App class.
 * 
 * @author mgilgar
 *
 */
public class AppTest {

	private App app;
	
	@Before
	public void setUp() throws IOException {
		app = new App();
	}
	
	@Test
	public void mainShouldWorkWhenNoArguments() throws IOException {
		app.main(new String[]{});
	}

}
