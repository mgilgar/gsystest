package org.mgilgar.gsystest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgilgar.gsystest.Game;
import org.mgilgar.gsystest.ResolvingTask;
import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.model.Player;
import org.mgilgar.gsystest.service.BetService;
import org.mgilgar.gsystest.service.PlayerService;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test class for {@link Game}.
 * 
 * @author mgilgar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GameTest {
	
	private static final String BETS_INPUT1 = "player1 1 2.0";
	private static final String BETS_INPUT_WRONG_BET = "player1 400 2.0";
	private static final String END_STRING = "";
	private static final String PLAYER_NAME1 = "player1";
	private static final String PLAYER_NAME2 = "player2";

	private Bet bet;
	private Game game;
	
	@Mock
	private BetService betService;
	
	@Mock
	private BufferedReader betsInput;
	
	@Mock
	private BufferedReader playersInput;
	
	@Mock
	private PlayerService playerService;
	
	@Mock
	private ResolvingTask resolvingTask;
	
	@Before
	public void setUp() {
		game = new Game();
		game.setBetService(betService);
		game.setPlayerService(playerService);
		game.setBetsInput(betsInput);
		game.setPlayersInput(playersInput);
		game.setResolvingTask(resolvingTask);
		List<Player> players = new ArrayList<Player>();
		players.add(new Player(PLAYER_NAME1));
		game.setPlayers(players);
		game.setBets(new ConcurrentLinkedQueue<Bet>());
		bet = new Bet();
		bet.setPlayer(new Player(PLAYER_NAME1));
	}

	@Test
	public void processBetsShouldAddBetWhenPlayerExists() throws IOException {
		when(betsInput.readLine()).thenReturn(BETS_INPUT1).thenReturn(END_STRING);
		when(betService.buildBet(BETS_INPUT1)).thenReturn(bet);
		game.processBets();
		assertEquals(1, game.getBets().size()); 
	}
	
	@Test
	public void processBetsShouldNotAddBetWhenPlayerDoesNotExist() throws IOException {
		when(betsInput.readLine()).thenReturn(BETS_INPUT1).thenReturn(END_STRING);
		bet.setPlayer(new Player(PLAYER_NAME2));
		when(betService.buildBet(BETS_INPUT1)).thenReturn(bet);
		game.processBets();
		assertEquals(0, game.getBets().size());
	}
	
	@Test
	public void processBetsShouldNotAddBetWhenBetIsNotAllowed() throws IOException {
		when(betsInput.readLine()).thenReturn(BETS_INPUT_WRONG_BET).thenReturn(END_STRING);
		bet.setPlayer(new Player(PLAYER_NAME2));
		when(betService.buildBet(BETS_INPUT_WRONG_BET)).thenReturn(null);
		game.processBets();
		assertEquals(0, game.getBets().size());
	}
	
	@Test
	public void playShouldInitilizeListOfPlayersAndBets() throws IOException {
		when(betsInput.readLine()).thenReturn(END_STRING);
		game.play();
		assertNotNull(game.getBets());
		assertEquals(0, game.getBets().size());
		assertNotNull(game.getPlayers());
		assertEquals(0, game.getPlayers().size());
	}
}
