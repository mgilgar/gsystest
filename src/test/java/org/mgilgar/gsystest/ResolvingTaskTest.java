package org.mgilgar.gsystest;

import static org.junit.Assert.assertEquals;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgilgar.gsystest.ResolvingTask;
import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.service.GameService;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test class for ResolvingTask.
 * 
 * @author mgilgar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ResolvingTaskTest {

	private Queue<Bet> bets;
	private ResolvingTask resolvingTask;

	@Mock
	private GameService gameService;
	
	@Before
	public void setUp() {
		bets = new ConcurrentLinkedQueue<Bet>();
		bets.add(new Bet());
		resolvingTask = new ResolvingTask();
		resolvingTask.setGameService(gameService);
		resolvingTask.setBets(bets);
	}
	
	@Test
	public void runShouldClearBets() {
		assertEquals(1, bets.size());
		resolvingTask.run();
		assertEquals(0, bets.size());
	}
}
