package org.mgilgar.gsystest.service;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgilgar.gsystest.model.Player;
import org.mgilgar.gsystest.service.PlayerService;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test for {@link PlayerService}.
 * @author mgilgar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {

	private static final String EMPTY_STRING = "";
	private static final String INPUT_LINE_PLAYER1 = "player1";
	private static final String INPUT_LINE_PLAYER2 = "player2";
	private static final String PLAYER1_NAME = INPUT_LINE_PLAYER1;
	private static final String PLAYER2_NAME = INPUT_LINE_PLAYER2;

	private PlayerService playerService;
	
	@Mock
	private BufferedReader bufferedReader;
	
	@Before
	public void setUp() {
		playerService = new PlayerService();
	}
	
	@Test
	public void getPlayersShouldReturnAListOfPlayers() throws IOException {
		List<Player> players = playerService.getPlayers(bufferedReader);
		assertNotNull(players);
	}
	
	@Test
	public void getPlayersShouldReturnEmptyListOfPlayersWhenEmptyInput() throws IOException {
		when(bufferedReader.readLine()).thenReturn(EMPTY_STRING);
		List<Player> players = playerService.getPlayers(bufferedReader);
		assertNotNull(players);
		assertEquals(players.size(), 0);
	}
	
	@Test
	public void getPlayersShouldReturnListOfPlayersWhenCorrectInput() throws IOException {
		when(bufferedReader.readLine()).thenReturn(INPUT_LINE_PLAYER1).thenReturn(INPUT_LINE_PLAYER2).thenReturn(EMPTY_STRING);
		List<Player> players = playerService.getPlayers(bufferedReader);
		assertNotNull(players);
		assertEquals(players.size(), 2);
		assertEquals(players.get(0).getName(), PLAYER1_NAME);
		assertEquals(players.get(1).getName(), PLAYER2_NAME);
	}
}
