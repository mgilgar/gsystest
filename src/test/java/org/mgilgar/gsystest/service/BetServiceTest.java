package org.mgilgar.gsystest.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.model.Player;
import org.mgilgar.gsystest.service.BetService;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test for {@link BetService}.
 * 
 * @author mgilgar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class BetServiceTest {

	private static final String EMPTY_STRING = "";
	private static final String INPUT_LINE_PLAYER1 = "player_1";
	private static final String PLAYER1_NAME = INPUT_LINE_PLAYER1;
	private static final String INPUT_LINE_BET1 = "player_1 1 1.0";
	private static final String INPUT_LINE_BET_NEGATIVE = "player_1 -1 1.0";
	private static final String INPUT_LINE_BET_BIGGER_THAN_MAX = "player_1 1000 1.0";
	private static final String INPUT_LINE_BET_EVEN_BET = "player_1 EVEN 1.0";
	private static final String INPUT_LINE_BET_ODD_BET = "player_1 ODD 1.0";
	private static final String INPUT_LINE_BET_INCOMPLETED = "player_1 1";
	private static final Player PLAYER1 = new Player(PLAYER1_NAME);
	private static final String BET1 = "1";
	private static final BigDecimal AMOUNT1 = BigDecimal.valueOf(1.0f);
	
	private BetService betService;
	
	@Mock
	private BufferedReader bufferedReader;
	
	@Before
	public void setUp() {
		betService = new BetService();
	}
	
	@Test
	public void buildBetShouldReturnNullWhenNull() {
		assertNull(betService.buildBet(null));
	}
	
	@Test
	public void buildBetShouldReturnNullWhenEmptyString() {
		assertNull(betService.buildBet(EMPTY_STRING));
	}
	
	@Test
	public void buildBetShouldReturnNullWhenInputDoesNotHave3Components() {
		assertNull(betService.buildBet(INPUT_LINE_BET_INCOMPLETED));
	}

	@Test
	public void buildBetShouldReturnWellFormedBetWhenCorrectInput() {
		Bet bet = betService.buildBet(INPUT_LINE_BET1);
		assertEquals(PLAYER1, bet.getPlayer());
		assertEquals(BET1, bet.getBet());
		assertEquals(AMOUNT1, bet.getAmount());
	}
	
	@Test
	public void buildBetShouldReturnNotNullWhenEvenBet() {
		assertNotNull(betService.buildBet(INPUT_LINE_BET_EVEN_BET));
	}
	
	@Test
	public void buildBetShouldReturnNotNullWhenOddBet() {
		assertNotNull(betService.buildBet(INPUT_LINE_BET_ODD_BET));		
	}
	
	@Test
	public void buildBetShouldReturnNullWhenNegativeNumber() {
		assertNull(betService.buildBet(INPUT_LINE_BET_NEGATIVE));
	}
	
	@Test
	public void buildBetShouldReturnNullWhenNumberBiggerThan36() {
		assertNull(betService.buildBet(INPUT_LINE_BET_BIGGER_THAN_MAX));
	}


}
