package org.mgilgar.gsystest.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mgilgar.gsystest.model.Bet;
import org.mgilgar.gsystest.model.Player;
import org.mgilgar.gsystest.service.GameService;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test for {@link GameService}.
 * 
 * @author mgilgar
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

	private static final String BET_LINE_OUTPUT_WIN = "player1\t2\tWIN\t36.0\n";
	private static final String BET_LINE_OUTPUT_LOSE = "player1\t2\tLOSE\t0.0\n";
	private static final String NO_BETS_OUTPUT = "There are no bets in this game.";

	private static final BigDecimal AMOUNT1 = BigDecimal.ONE;
	private static final BigDecimal AMOUNT2 = BigDecimal.valueOf(2);
	private static final BigDecimal AMOUNT3 = BigDecimal.valueOf(3);
	private static final BigDecimal AMOUNT_36X = BigDecimal.valueOf(36);
	private static final BigDecimal NO_WINNINGS_AMOUNT = BigDecimal.ZERO;

	private static final String BET1 = "1";
	private static final String BET2 = "2";
	private static final String BET_EVEN = "EVEN";
	private static final String BET_ODD = "ODD";
	private static final String PLAYER_NAME1 = "player1";
	private static final String PLAYER_NAME2 = "player2";
	private static final String PLAYER_NAME3 = "player3";
	private static final Player PLAYER1 = new Player(PLAYER_NAME1);
	private static final Player PLAYER2 = new Player(PLAYER_NAME2);
	private static final Player PLAYER3 = new Player(PLAYER_NAME3);
	
	private static final int NUMBER1 = 1;
	private static final int NUMBER2 = 2;

	private static final int MAXIMUM_NUMBER = 36;
	
	private static final Object ONE_BET_OUTPUT = "Number: 2\n" 
			+ "Player\tBet\tOutcome\tWinnings\n"
			+ "---\n"
			+ "player1\t1\tLOSE\t0.0\n";
	private static final Object THREE_BET_OUTPUT = "Number: 2\n" 
			+ "Player\tBet\tOutcome\tWinnings\n"
			+ "---\n"
			+ "player1\t1\tLOSE\t0.0\n"
			+ "player2\t2\tWIN\t72.0\n"
			+ "player3\tEVEN\tWIN\t6.0\n";
	
	private Bet bet1;
	private Bet bet2;
	private Bet bet3;
	private Queue<Bet> bets;
	private GameService gameService; 
	
	@Mock
	private Random random;
	
	@Before
	public void setUp() {
		gameService = new GameService();
		gameService.setRamdon(random);
		bets = new ConcurrentLinkedQueue<Bet>();
		bet1 = new Bet();
		bet1.setAmount(AMOUNT1);
		bet1.setBet(BET1);
		bet1.setPlayer(PLAYER1);
	}
	
	@Test
	public void resolveWithNullBetsShouldReturnNoBets() {
		String output = gameService.resolve(null);
		assertEquals(NO_BETS_OUTPUT, output);
	}
	
	@Test 
	public void resolveWithEmptyBetsShouldReturnNoBets() {
		String output = gameService.resolve(bets);
		assertEquals(NO_BETS_OUTPUT, output);	
	}
	
	@Test
	public void resolveWithOneNonWinningBetShouldReturnNoWinnersPlusThatBet() {
		bets.add(bet1);
		when(random.nextInt(MAXIMUM_NUMBER)).thenReturn(1);
		String output = gameService.resolve(bets);
		assertEquals(ONE_BET_OUTPUT, output);	
	}
	
	@Test
	public void resolveWithThreeBetsShouldReturnCorrespondingOutput() {
		bet2 = new Bet();
		bet2.setAmount(AMOUNT2);
		bet2.setBet(BET2);
		bet2.setPlayer(PLAYER2);
		bet3 = new Bet();
		bet3.setAmount(AMOUNT3);
		bet3.setBet(BET_EVEN);
		bet3.setPlayer(PLAYER3);
		bets.add(bet1);
		bets.add(bet2);
		bets.add(bet3);
		when(random.nextInt(MAXIMUM_NUMBER)).thenReturn(1);
		String output = gameService.resolve(bets);
		assertEquals(THREE_BET_OUTPUT, output);	
	}
	
	@Test
	public void resolveOneBetShouldReturnLOSEoutputWhenLosingBet() {
		bet1.setBet(BET2);
		StringBuilder sb = gameService.resolve(bet1, NUMBER1);
		assertEquals(BET_LINE_OUTPUT_LOSE, sb.toString());
	}
	
	@Test
	public void resolveOneBetShouldReturnWINoutputWhenWinningBet() {
		bet1.setBet(BET2);
		StringBuilder sb = gameService.resolve(bet1, NUMBER2);
		assertEquals(BET_LINE_OUTPUT_WIN, sb.toString());
	}
	
	@Test
	public void getWinningsShouldReturnZeroWinningsWhenNoWinningBet() {
		bet1.setBet(BET1);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER2);
		assertEquals(NO_WINNINGS_AMOUNT, winnings);
	}

	@Test
	public void getWinningsShouldReturnDoubleAmountWhenEvenBetAndEvenNumber() {
		bet1.setBet(BET_EVEN);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER2);
		assertEquals(bet1.getAmount().multiply(BigDecimal.valueOf(2)), winnings);
	}
	
	@Test
	public void getWinningsShouldReturnZeroAmountWhenEvenBetAndOddNumber() {
		bet1.setBet(BET_EVEN);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER1);
		assertEquals(BigDecimal.ZERO, winnings);
	}
	
	@Test
	public void getWinningsShouldReturnDoubleAmountWhenOddBetAndOddNumber() {
		bet1.setBet(BET_ODD);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER1);
		assertEquals(bet1.getAmount().multiply(BigDecimal.valueOf(2)), winnings);
	}
	
	@Test
	public void getWinningsShouldReturnZeroAmountWhenOddBetAndEvenNumber() {
		bet1.setBet(BET_ODD);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER2);
		assertEquals(BigDecimal.ZERO, winnings);
	}
	
	@Test
	public void getWinningsShouldReturn36TimesAmountWhenWinningsBet() {
		bet1.setBet(BET2);
		BigDecimal winnings = gameService.getWinnings(bet1, NUMBER2);
		assertEquals(AMOUNT1.multiply(AMOUNT_36X), winnings);
	}
}
